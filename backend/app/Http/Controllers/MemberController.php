<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Member;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
class MemberController extends Controller
{
    public function getData(){
        
        try{
            $members = DB::table('members')->get();
            return response([
                'message'=> "Data get successfully.",
                'members'=> $members
            ]);
        }catch(Exception $ex){[
            'message'=>$ex->getMessage()];
        }
    }

    public function saveData(Request $req){
        $validation = Validator::make($req->all(), [
            'name' => 'required',
            'ocupation' => 'required',
            'mobile_num' => 'required',
            'company_name' => 'required',
            'district' => 'required',
        ]);
        if($validation->fails()){
            return response([
                'message' => $validation->errors()->all()
            ]);
        }
        try{
           
            $member = new Member();
            $member->name = $req->name;
            $member->ocupation = $req->ocupation;
            $member->mobile_num = $req->mobile_num;
            $member->company_name = $req->company_name;
            $member->district = $req->district;
            $member->join_date = date('Y-m-d', strtotime($req->join_date));
            if($req->hasFile('image')){
                $file = $req->file('image');
                $fileName = $file->getclientoriginalname();
                $name = date('His').time().'-'.$fileName;
                $file_path = $file->move(public_path('upload'), $name);
                $member->image_name = $name;
                $member->image_path = $file_path;
             
            }
            $member->save();

            return response([
                'message'=>"Save successfully",
                'member'=>$member
            ]);
        }catch(Exception $ex){
        ['message'=>$ex->getMessage()];
        }
    }
    public function updateData(Request $req, $id){
        try{
            $member = Member::find($id);
            $member->name = $req->name;
            $member->ocupation = $req->ocupation;
            $member->mobile_num = $req->mobile_num;
            $member->company_name = $req->company_name;
            $member->district = $req->district;
            $member->join_date = date('Y-m-d', strtotime($req->join_date));
            if($req->hasFile('image')){
                $file = $req->file('image');
                $fileName = $file->getclientoriginalname();
                $name = date('His').time().'-'.$fileName;
                $file_path = $file->move(public_path('upload'), $name);
                $member->image_name = $name;
                $member->image_path = $file_path;
             
            }
            $member->save();

            return response([
                'message'=> "Updated successfully",
                'member'=> $member,
            ]);
        }catch(Throwable $th){
            return response(
            ['message'=>$th->getMessage()]);
        }
    }

    public function deleteData($id){
        try{
            $member = Member::find($id);
            $member->delete();

            return response([
                'message'=> 'Data Delete Succesfully',
                'member'=> $member,
            ]);
        }catch(Exception $ex){
                return response([
                    'message' => $ex->getMessage()
                ]);
        
        }
    }

}
