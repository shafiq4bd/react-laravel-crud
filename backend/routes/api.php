<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\MemberController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});


Route::get('/members', [MemberController::class, 'getData']);
Route::post('/members', [MemberController::class, 'saveData']);
Route::put('/members/{id}', [MemberController::class, 'updateData']);
Route::delete('/members/{id}', [MemberController::class, 'deleteData']);
